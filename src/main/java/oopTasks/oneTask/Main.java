package oopTasks.oneTask;

public class Main {
    public static void main(String[] args) {
        SamsungPhone samsungPhone = new SamsungPhone("Samsung", "A21", 256, 6);
        samsungPhone.call();
        samsungPhone.sendMessage();
        samsungPhone.makePhoto();
        samsungPhone.makeVideo();
        NokiaPhone nokiaPhone = new NokiaPhone("Nokia", "3201", 16, 1);
        nokiaPhone.call();
        nokiaPhone.sendMessage();

    }
}
