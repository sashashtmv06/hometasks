package oopTasks.oneTask;

public interface PhoneMedia {
    void makePhoto();

    void makeVideo();
}
