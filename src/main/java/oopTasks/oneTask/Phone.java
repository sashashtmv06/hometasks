package oopTasks.oneTask;

public abstract class Phone implements PhoneConnection {
    private String name;
    private String model;
    private int dataHdd;
    private int dataRam;

    public Phone(String name, String model, int dataHdd, int dataRam) {
        this.name = name;
        this.model = model;
        this.dataHdd = dataHdd;
        this.dataRam = dataRam;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getDataHdd() {
        return dataHdd;
    }

    public void setDataHdd(int dataHdd) {
        this.dataHdd = dataHdd;
    }

    public int getDataRam() {
        return dataRam;
    }

    public void setDataRam(int dataRam) {
        this.dataRam = dataRam;
    }
}
