package oopTasks.twoTask.com.company.vehicles;

import oopTasks.twoTask.com.company.details.Engine;
import oopTasks.twoTask.com.company.professions.Driver;

public class SportCar extends Car{
    private int limitSpeed;

    public int getLimitSpeed() {
        return limitSpeed;
    }

    public void setLimitSpeed(int limitSpeed) {
        this.limitSpeed = limitSpeed;
    }

    public SportCar(String carModel, String classModel, int weight, Driver driver, Engine engine) {
        super(carModel, classModel, weight, driver, engine);
    }
}
