package oopTasks.twoTask.com.company.vehicles;

import oopTasks.twoTask.com.company.details.Engine;
import oopTasks.twoTask.com.company.professions.Driver;

public class Car {
    private String carModel;
    private String classModel;
    private int weight;
    private Driver driver;
    private Engine engine;


    public Car(String carModel, String classModel, int weight, Driver driver, Engine engine) {
        this.carModel = carModel;
        this.classModel = classModel;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getClassModel() {
        return classModel;
    }

    public void setClassModel(String classModel) {
        this.classModel = classModel;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void start() {
        System.out.println("Поїхали");
    }

    public void stop() {
        System.out.println("Зупиняємося");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот наліво");
    }

    @Override
    public String toString() {
        return "Авто моделі - " + getCarModel() + ", класу - " + getClassModel() + ", вагою - " + getWeight() + ", з водієм - " +
                getDriver().getFirstName() + " " + getDriver().getLastName() + ", полу - " + getDriver().getGender() +
                ", стажем водіння - " + getDriver().getDrivingExperience() + " та телефонним номером - " + getDriver().getTelephoneNumber();
    }
}
