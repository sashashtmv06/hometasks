package oopTasks.twoTask.com.company.vehicles;

import oopTasks.twoTask.com.company.details.Engine;
import oopTasks.twoTask.com.company.professions.Driver;

public class Lorry extends Car{
    private String loadCapacity;

    public String getLoadCapacity() {
        return loadCapacity;
    }

    public void setLoadCapacity(String loadCapacity) {
        this.loadCapacity = loadCapacity;
    }

    public Lorry(String carModel, String classModel, int weight, Driver driver, Engine engine) {
        super(carModel, classModel, weight, driver, engine);
    }
}
