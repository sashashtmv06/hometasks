package oopTasks.twoTask.com.company.professions;

import oopTasks.twoTask.com.company.company.Person;

public class Driver extends Person {
    private String drivingExperience;

    public String getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(String drivingExperience) {
        this.drivingExperience = drivingExperience;
    }

    public Driver(String firstName, String lastName, int age, String gender, String telephoneNumber) {
        super(firstName, lastName, age, gender, telephoneNumber);
    }
}
