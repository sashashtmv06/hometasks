package shapesCalculator.models;

public abstract class Shape {

    public abstract double getArea();

    public abstract String getName();

    @Override
    public String toString() {
        return "[Фігура: " + getName() + " площею " + getArea() + "]";
    }
}

